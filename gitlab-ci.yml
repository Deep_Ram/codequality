image: trion/ng-cli-karma:13.3.6

default:
  image: ruby:2.7.2


services:
  - name: docker:20-dind
    alias: docker
    command: ["--tls=false"]

variables:
    DOCKER_HOST: tcp://docker:2375
    DOCKER_DRIVER: overlay2
    DOCKER_TLS_CERTDIR: ""
    SMILE_VERSION: 
        value: "2022.11.PRE"
        description: "Build AG UI webjar to match Smile CDR version and use it as Git tag for master branch build"

workflow:
    rules:
        - if: $CI_COMMIT_MESSAGE =~ /-draft$/
          when: never
        - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
        - if: '$CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS'
          when: never
        - if: '$CI_COMMIT_BRANCH'

stages:
    - build
    - lint
    - test
    - code_quality
    - deploy
    - e2e

# --------------------------------------------------------------------
# Init jobs

cache:
    key: ${CI_COMMIT_REF_SLUG}
    paths:
        - .npm/

before_script:
    - echo "npm cache settings..."
    - npm ci --cache .npm --prefer-offline

# --------------------------------------------------------------------
# Build compile jobs

build_console:
    tags: 
        - lightweight
    stage: build
    cache:
        policy: pull
    script:
        - echo "Build console..."
        - ng build console
    interruptible: true

build_console_marketplace:
    tags: 
        - lightweight
    stage: build
    cache:
        policy: pull
    script:
        - echo "Build console-marketplace..."
        - ng build console-marketplace
    interruptible: true

build_portal:
    tags: 
        - lightweight
    stage: build
    cache:
        policy: pull
    script:
        - echo "Build portal..."
        - ng build portal
    interruptible: true

build_portal_marketplace:
    tags: 
        - lightweight
    stage: build
    cache:
        policy: pull
    script:
        - echo "Build portal-marketplace..."
        - ng build portal-marketplace
    interruptible: true

build_gallery:
    tags: 
        - lightweight
    stage: build
    cache:
        policy: pull
    script:
        - echo "Build gallery..."
        - ng build gallery
    interruptible: true

build_gallery_marketplace:
    tags: 
        - lightweight
    stage: build
    cache:
        policy: pull
    script:
        - echo "Build gallery-marketplace..."
        - ng build gallery-marketplace
    interruptible: true

# --------------------------------------------------------------------
# npm audit

npm_audit:
    tags: 
        - lightweight
    stage: build
    cache:
        policy: pull
    script:
        - npm run audit
    interruptible: true

# --------------------------------------------------------------------
# ESlint jobs

eslint_environments:
    tags: 
        - lightweight
    stage: lint
    cache:
        policy: pull
    script:
        - echo "ESLint environments..."
        - ng lint environments 2>&1 | tee lint_output.txt
        - ls lint_output.txt
    interruptible: true
    artifacts:
        paths:
            - lint_output.txt

eslint_models:
    tags: 
        - lightweight
    stage: lint
    cache:
        policy: pull
    script:
        - echo "ESLint models..."
        - ng lint models 2>&1 | tee lint_output.txt
        - ls lint_output.txt
    interruptible: true
    artifacts:
        paths:
            - lint_output.txt

eslint_common:
    tags: 
        - lightweight
    stage: lint
    cache:
        policy: pull
    script:
        - echo "ESLint common..."
        - ng lint common 2>&1 | tee lint_output.txt
        - ls lint_output.txt
    interruptible: true
    artifacts:
        paths:
            - lint_output.txt

eslint_common_appsphere:
    tags: 
        - lightweight
    stage: lint
    cache:
        policy: pull
    script:
        - echo "ESLint common-appsphere..."
        - ng lint common-appsphere 2>&1 | tee lint_output.txt
        - ls lint_output.txt
    interruptible: true
    artifacts:
        paths:
            - lint_output.txt

eslint_common_marketplace:
    tags: 
        - lightweight
    stage: lint
    cache:
        policy: pull
    script:
        - echo "ESLint common-marketplace..."
        - ng lint common-marketplace 2>&1 | tee lint_output.txt
        - ls lint_output.txt
    interruptible: true
    artifacts:
        paths:
            - lint_output.txt

eslint_common_portal:
    tags: 
        - lightweight
    stage: lint
    cache:
        policy: pull
    script:
        - echo "ESLint common..."
        - ng lint common-portal 2>&1 | tee lint_output.txt
        - ls lint_output.txt
    interruptible: true
    artifacts:
        paths:
            - lint_output.txt

eslint_common_gallery:
    tags: 
        - lightweight
    stage: lint
    cache:
        policy: pull
    script:
        - echo "ESLint common..."
        - ng lint common-gallery 2>&1 | tee lint_output.txt
        - ls lint_output.txt
    interruptible: true
    artifacts:
        paths:
            - lint_output.txt

eslint_common_console:
    tags: 
        - lightweight
    stage: lint
    cache:
        policy: pull
    script:
        - echo "ESLint common..."
        - ng lint common-console 2>&1 | tee lint_output.txt
        - ls lint_output.txt
    interruptible: true
    artifacts:
        paths:
            - lint_output.txt

eslint_console:
    tags: 
        - lightweight
    stage: lint
    cache:
        policy: pull
    script:
        - echo "ESLint console..."
        - ng lint console 2>&1 | tee lint_output.txt
        - ls lint_output.txt
    interruptible: true
    artifacts:
        paths:
            - lint_output.txt

eslint_console_marketplace:
    tags: 
        - lightweight
    stage: lint
    cache:
        policy: pull
    script:
        - echo "ESLint console-marketplace..."
        - ng lint console-marketplace 2>&1 | tee lint_output.txt
        - ls lint_output.txt
    interruptible: true
    artifacts:
        paths:
            - lint_output.txt

eslint_portal:
    tags: 
        - lightweight
    stage: lint
    cache:
        policy: pull
    script:
        - echo "ESLint portal..."
        - ng lint portal 2>&1 | tee lint_output.txt
        - ls lint_output.txt
    interruptible: true
    artifacts:
        paths:
            - lint_output.txt

eslint_portal_marketplace:
    tags: 
        - lightweight
    stage: lint
    cache:
        policy: pull
    script:
        - echo "ESLint portal-marketplace..."
        - ng lint portal-marketplace 2>&1 | tee lint_output.txt
        - ls lint_output.txt
    interruptible: true
    artifacts:
        paths:
            - lint_output.txt

eslint_gallery:
    tags: 
        - lightweight
    stage: lint
    cache:
        policy: pull
    script:
        - echo "ESLint gallery..."
        - ng lint gallery 2>&1 | tee lint_output.txt
        - ls lint_output.txt
    interruptible: true
    artifacts:
        paths:
            - lint_output.txt

eslint_gallery_marketplace:
    tags: 
        - lightweight
    stage: lint
    cache:
        policy: pull
    script:
        - echo "ESLint gallery-marketplace..."
        - ng lint gallery-marketplace 2>&1 | tee lint_output.txt
        - ls lint_output.txt
    interruptible: true
    artifacts:
        paths:
            - lint_output.txt

eslint_console_e2e:
    tags: 
        - lightweight
    stage: lint
    cache:
        policy: pull
    script:
        - echo "ESLint console-e2e..."
        - ng lint console-e2e 2>&1 | tee lint_output.txt
        - ls lint_output.txt
    interruptible: true
    artifacts:
        paths:
            - lint_output.txt

eslint_console_marketplace_e2e:
    tags: 
        - lightweight
    stage: lint
    cache:
        policy: pull
    script:
        - echo "ESLint console-marketplace-e2e..."
        - ng lint console-marketplace-e2e 2>&1 | tee lint_output.txt
        - ls lint_output.txt
    interruptible: true
    artifacts:
        paths:
            - lint_output.txt

eslint_portal_e2e:
    tags: 
        - lightweight
    stage: lint
    cache:
        policy: pull
    script:
        - echo "ESLint portal-e2e..."
        - ng lint portal-e2e 2>&1 | tee lint_output.txt
        - ls lint_output.txt
    interruptible: true
    artifacts:
        paths:
            - lint_output.txt

eslint_portal_marketplace_e2e:
    tags: 
        - lightweight
    stage: lint
    cache:
        policy: pull
    script:
        - echo "ESLint portal-marketplace-e2e..."
        - ng lint portal-marketplace-e2e 2>&1 | tee lint_output.txt
        - ls lint_output.txt
    interruptible: true
    artifacts:
        paths:
            - lint_output.txt


eslint_gallery_e2e:
    tags: 
        - lightweight
    stage: lint
    cache:
        policy: pull
    script:
        - echo "ESLint gallery-e2e..."
        - ng lint gallery-e2e 2>&1 | tee lint_output.txt
        - ls lint_output.txt
    interruptible: true
    artifacts:
        paths:
            - lint_output.txt
            
eslint_gallery_marketplace_e2e:
    tags: 
        - lightweight
    stage: lint
    cache:
        policy: pull
    script:
        - echo "ESLint gallery-marketplace-e2e..."
        - ng lint gallery-marketplace-e2e 2>&1 | tee lint_output.txt
        - ls lint_output.txt
    interruptible: true
    artifacts:
        paths:
            - lint_output.txt

# --------------------------------------------------------------------
# Test jobs

test_unit_common:
    tags: 
        - lightweight
    stage: test
    cache:
        policy: pull
    script:
        - echo "Unit tests for common library..."
        - npm run test-headless -- --project=common
    interruptible: true
    coverage: '/Statements\s*:\s*([^%]+)%/'
    artifacts:
        when: always    
        paths:
            - coverage/
        reports:
            junit:
                - libs/common/junit.xml
            coverage_report:
                coverage_format: cobertura
                path: coverage/common/cobertura-coverage.xml

test_unit_common_appsphere:
    tags: 
        - lightweight
    stage: test
    cache:
        policy: pull
    script:
        - echo "Unit tests for common-appsphere library..."
        - npm run test-headless -- --project=common-appsphere
    interruptible: true
    coverage: '/Statements\s*:\s*([^%]+)%/'
    artifacts:
        when: always    
        paths:
            - coverage/
        reports:
            junit:
                - libs/common-appsphere/junit.xml
            coverage_report:
                coverage_format: cobertura
                path: coverage/common-appsphere/cobertura-coverage.xml

test_unit_common_marketplace:
    tags: 
        - lightweight
    stage: test
    cache:
        policy: pull
    script:
        - echo "Unit tests for common-marketplace library..."
        - npm run test-headless -- --project=common-marketplace
    interruptible: true
    coverage: '/Statements\s*:\s*([^%]+)%/'
    artifacts:
        when: always    
        paths:
            - coverage/
        reports:
            junit:
                - libs/common-marketplace/junit.xml
            coverage_report:
                coverage_format: cobertura
                path: coverage/common-marketplace/cobertura-coverage.xml

test_unit_common_portal:
    tags: 
        - lightweight
    stage: test
    cache:
        policy: pull
    script:
        - echo "Unit tests for common portal library..."
        - npm run test-headless -- --project=common-portal
    interruptible: true
    coverage: '/Statements\s*:\s*([^%]+)%/'
    artifacts:
        when: always    
        paths:
            - coverage/
        reports:
            junit:
                - libs/common-portal/junit.xml
            coverage_report:
                coverage_format: cobertura
                path: coverage/common-portal/cobertura-coverage.xml

test_unit_common_gallery:
    tags: 
        - lightweight
    stage: test
    cache:
        policy: pull
    script:
        - echo "Unit tests for common gallery library..."
        - npm run test-headless -- --project=common-gallery
    interruptible: true
    coverage: '/Statements\s*:\s*([^%]+)%/'
    artifacts:
        when: always    
        paths:
            - coverage/
        reports:
            junit:
                - libs/common-gallery/junit.xml
            coverage_report:
                coverage_format: cobertura
                path: coverage/common-gallery/cobertura-coverage.xml

test_unit_common_console:
    tags: 
        - lightweight
    stage: test
    cache:
        policy: pull
    script:
        - echo "Unit tests for common console library..."
        - npm run test-headless -- --project=common-console
    interruptible: true
    coverage: '/Statements\s*:\s*([^%]+)%/'
    artifacts:
        when: always    
        paths:
            - coverage/
        reports:
            junit:
                - libs/common-console/junit.xml
            coverage_report:
                coverage_format: cobertura
                path: coverage/common-console/cobertura-coverage.xml

test_unit_environments:
    tags: 
        - lightweight
    stage: test
    cache:
        policy: pull
    script:
        - echo "Unit tests for environments library..."
        - npm run test-headless -- --project=environments
    interruptible: true
    coverage: '/Statements\s*:\s*([^%]+)%/'
    artifacts:
        when: always    
        paths:
            - coverage/
        reports:
            junit:
                - libs/environments/junit.xml
            coverage_report:
                coverage_format: cobertura
                path: coverage/environments/cobertura-coverage.xml

test_unit_models:
    tags: 
        - lightweight
    stage: test
    cache:
        policy: pull
    script:
        - echo "Unit tests for models library..."
        - npm run test-headless -- --project=models
    interruptible: true
    coverage: '/Statements\s*:\s*([^%]+)%/'
    artifacts:
        when: always    
        paths:
            - coverage/
        reports:
            junit:
                - libs/models/junit.xml
            coverage_report:
                coverage_format: cobertura
                path: coverage/models/cobertura-coverage.xml

test_unit_portal:
    tags: 
        - lightweight
    stage: test
    cache:
        policy: pull
    script:
        - echo "Unit test for portal project..."
        - npm run test-headless -- --project=portal
    interruptible: true
    coverage: '/Statements\s*:\s*([^%]+)%/'
    artifacts:
        when: always    
        paths:
            - coverage/
        reports:
            junit:
                - apps/portal/junit.xml
            coverage_report:
                coverage_format: cobertura
                path: coverage/portal/cobertura-coverage.xml

test_unit_portal_marketplace:
    tags: 
        - lightweight
    stage: test
    cache:
        policy: pull
    script:
        - echo "Unit test for portal-marketplace project..."
        - npm run test-headless -- --project=portal-marketplace
    interruptible: true
    coverage: '/Statements\s*:\s*([^%]+)%/'
    artifacts:
        when: always    
        paths:
            - coverage/
        reports:
            junit:
                - apps/portal-marketplace/junit.xml
            coverage_report:
                coverage_format: cobertura
                path: coverage/portal-marketplace/cobertura-coverage.xml

test_unit_gallery:
    tags: 
        - lightweight
    stage: test
    cache:
        policy: pull
    script:
        - echo "Unit test for gallery project..."
        - npm run test-headless -- --project=gallery
    interruptible: true
    coverage: '/Statements\s*:\s*([^%]+)%/'
    artifacts:
        when: always    
        paths:
            - coverage/
        reports:
            junit:
                - apps/gallery/junit.xml
            coverage_report:
                coverage_format: cobertura
                path: coverage/gallery/cobertura-coverage.xml

test_unit_gallery_marketplace:
    tags: 
        - lightweight
    stage: test
    cache:
        policy: pull
    script:
        - echo "Unit test for gallery-marketplace project..."
        - npm run test-headless -- --project=gallery-marketplace
    interruptible: true
    coverage: '/Statements\s*:\s*([^%]+)%/'
    artifacts:
        when: always    
        paths:
            - coverage/
        reports:
            junit:
                - apps/gallery-marketplace/junit.xml
            coverage_report:
                coverage_format: cobertura
                path: coverage/gallery-marketplace/cobertura-coverage.xml

test_unit_console:
    tags: 
        - lightweight
    stage: test
    cache:
        policy: pull
    script:
        - echo "Unit test for console project..."
        - npm run test-headless -- --project=console
    interruptible: true
    coverage: '/Statements\s*:\s*([^%]+)%/'
    artifacts:
        when: always    
        paths:
            - coverage/
        reports:
            junit:
                - apps/console/junit.xml
            coverage_report:
                coverage_format: cobertura
                path: coverage/console/cobertura-coverage.xml

test_unit_console_marketplace:
    tags: 
        - lightweight
    stage: test
    cache:
        policy: pull
    script:
        - echo "Unit test for console-marketplace project..."
        - npm run test-headless -- --project=console-marketplace
    interruptible: true
    coverage: '/Statements\s*:\s*([^%]+)%/'
    artifacts:
        when: always    
        paths:
            - coverage/
        reports:
            junit:
                - apps/console-marketplace/junit.xml
            coverage_report:
                coverage_format: cobertura
                path: coverage/console-marketplace/cobertura-coverage.xml

# --------------------------------------------------------------------
# e2e jobs

test_e2e:
    tags: 
        - lightweight
    stage: e2e
    script:
        - echo "e2e/cypress tests..."
        - npm run ci:e2e-portal
        - npm run ci:e2e-console
        - npm run ci:e2e-gallery
    artifacts:
        when: always    
        paths:
            - dist/cypress
    interruptible: true
    rules:
        - if: '$CI_COMMIT_BRANCH == "feature" && $CI_PIPELINE_SOURCE == "schedule"'
        - if: '$CI_COMMIT_BRANCH'
          when: manual

test_e2e_marketplace:
    tags: 
        - lightweight
    stage: e2e
    script:
        - echo "e2e/cypress tests..."
        - npm run ci:e2e-portal-marketplace
        - npm run ci:e2e-console-marketplace
        - npm run ci:e2e-gallery-marketplace
    artifacts:
        when: always    
        paths:
            - dist/cypress
    interruptible: true
    rules:
        - if: '$CI_COMMIT_BRANCH == "feature" && $CI_PIPELINE_SOURCE == "schedule"'
        - if: '$CI_COMMIT_BRANCH'
          when: manual

# --------------------------------------------------------------------
# Quality jobs

include:
    - template: Code-Quality.gitlab-ci.yml

code_quality:
    tags:
        - gitlab-org-docker
    stage: code_quality
    before_script:
        - echo "Code quality..."
        - cat .codeclimate.yml
    cache: {}
    after_script:
        - ls gl-code-quality-report*
    artifacts:
        paths: [gl-code-quality-report.json]
    rules:
        - if: '$CODE_QUALITY_DISABLED'
          when: never
        - if: $CI_COMMIT_MESSAGE =~ /-draft$/
          when: never
        - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
        - if: '$CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS'
          when: never
        - if: '$CI_COMMIT_BRANCH == "feature" && $CI_PIPELINE_SOURCE == "schedule"'
          when: never
        - if: '$CI_COMMIT_BRANCH'

code_quality_html:
    extends: code_quality
    variables:
        REPORT_FORMAT: html
    artifacts:
        paths: [gl-code-quality-report.html]
    rules:
        - if: '$CI_COMMIT_BRANCH == "feature" && $CI_PIPELINE_SOURCE == "schedule"'
          when: never
        - if: '$CI_COMMIT_BRANCH == "feature"'

sonarqube-check:
    tags: 
        - lightweight
    stage: code_quality        
    image: 
        name: sonarsource/sonar-scanner-cli:4.6
        entrypoint: [""]
    variables:
        SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"  # Defines the location of the analysis task cache
        GIT_DEPTH: "0"  # Tells git to fetch all the branches of the project, required by the analysis task
    cache:
        key: "${CI_JOB_NAME}"
        paths:
            - .sonar/cache
    script: 
        - sonar-scanner
    interruptible: true
    needs:
        - test_unit_common
        - test_unit_common_appsphere
        - test_unit_common_marketplace
        - test_unit_common_gallery
        - test_unit_common_portal
        - test_unit_common_console
        - test_unit_environments
        - test_unit_models
        - test_unit_portal
        - test_unit_gallery
        - test_unit_console     
        - test_unit_gallery_marketplace 
        - test_unit_portal_marketplace
        - test_unit_console_marketplace
    allow_failure: true

# --------------------------------------------------------------------
# Artifact jobs

build_maven_artifacts:
    tags: 
        - lightweight
    stage: deploy
    cache:
        policy: pull
    script:
        - echo "Create Maven artifacts..."
        - git config user.name "Gitlab CI Bot"
        - git config user.email "<ci@smilecdr.com>"
        - npm version --nogit-tag-version prepatch --preid=${CI_PIPELINE_ID}
        - echo "Build webjar to maven repository..."
        - npx nx run-many --target=build --all=true --configuration=pre-production
        - cd webjar
        - ./buildwebjar-appgallery.sh build ${SMILE_VERSION}
        - ./buildwebjar-marketplace.sh build ${SMILE_VERSION}
        - cd ..
        - PACKAGE_VERSION=`cat package.json | grep "\"version\":" | cut -d "\"" -f 4`
        - echo "AG UI feature branch - ${PACKAGE_VERSION} ${SMILE_VERSION}"
    interruptible: true
    artifacts:
        paths:
            - webjar/dist/cdr-appgallery-ui.jar
            - webjar/dist/cdr-marketplace-ui.jar
    rules:
        - if: '$CI_COMMIT_BRANCH == "feature" && $CI_PIPELINE_SOURCE == "schedule"'
          when: never
        - if: '$CI_COMMIT_BRANCH == "feature"'

build_docker_artifacts:
    tags: 
        - lightweight
    stage: deploy
    cache:
        policy: pull
    script:
        - echo "Create Docker artifacts..."
        - git config user.name "Gitlab CI Bot"
        - git config user.email "<ci@smilecdr.com>"
        - npm version --nogit-tag-version prepatch --preid=${CI_PIPELINE_ID}
        - npx nx run-many --target=build --all=true
        - npm run zip-package
    interruptible: true
    artifacts:
        paths:
            - dist/
    rules:
        - if: '$CI_COMMIT_BRANCH == "feature" && $CI_PIPELINE_SOURCE == "schedule"'
          when: never
        - if: '$CI_COMMIT_BRANCH == "feature"'

# --------------------------------------------------------------------
# Artifact deploy

deploy_maven_artifacts:
    tags: 
        - lightweight
    stage: deploy
    image: maven:3.8.1-jdk-11
    before_script:
        - echo "Skipping global before_script"
    needs:
        - build_maven_artifacts
    script:
        - echo "Deploy webjar to maven repository..."
        - cd webjar
        - ./buildwebjar-appgallery.sh deploy ${SMILE_VERSION}
        - ./buildwebjar-marketplace.sh deploy ${SMILE_VERSION}
    needs:
        - build_maven_artifacts
    rules:
        - if: '$CI_COMMIT_BRANCH == "feature" && $CI_PIPELINE_SOURCE == "schedule"'
          when: never
        - if: '$CI_COMMIT_BRANCH == "feature"'

deploy_docker_artifacts:
    tags: 
        - lightweight
    stage: deploy
    cache:
        policy: pull
    image: docker:stable
    before_script:
        - docker info
    script:
        - PACKAGE_VERSION=`cat package.json | grep "\"version\":" | cut -d "\"" -f 4`
        - CONSOLE_IMAGE_NAME="${SMILECDR_DOCKER_REGISTRY}${SMILECDR_DOCKER_REGISTRY_PATH}ag_console"
        - PORTAL_IMAGE_NAME="${SMILECDR_DOCKER_REGISTRY}${SMILECDR_DOCKER_REGISTRY_PATH}ag_portal"
        - GALLERY_IMAGE_NAME="${SMILECDR_DOCKER_REGISTRY}${SMILECDR_DOCKER_REGISTRY_PATH}ag_gallery"
        - echo "Build docker images..."
        - docker build . -t "${CONSOLE_IMAGE_NAME}":dev-latest -t "${CONSOLE_IMAGE_NAME}":dev-"${PACKAGE_VERSION}" -f dockerfiles/Dockerfile.console
        - docker build . -t "${PORTAL_IMAGE_NAME}":dev-latest -t "${PORTAL_IMAGE_NAME}":dev-"${PACKAGE_VERSION}" -f dockerfiles/Dockerfile.portal
        - docker build . -t "${GALLERY_IMAGE_NAME}":dev-latest -t "${GALLERY_IMAGE_NAME}":dev-"${PACKAGE_VERSION}" -f dockerfiles/Dockerfile.gallery
        - echo "Push docker images..."
        - docker -D login -u="${SMILECDR_DOCKER_REPO_USER}" -p="${SMILECDR_DOCKER_REPO_API_KEY}" "${SMILECDR_DOCKER_REGISTRY}"
        - docker push "${CONSOLE_IMAGE_NAME}":dev-latest
        - docker push "${CONSOLE_IMAGE_NAME}":dev-"${PACKAGE_VERSION}"
        - docker push "${PORTAL_IMAGE_NAME}":dev-latest
        - docker push "${PORTAL_IMAGE_NAME}":dev-"${PACKAGE_VERSION}"
        - docker push "${GALLERY_IMAGE_NAME}":dev-latest
        - docker push "${GALLERY_IMAGE_NAME}":dev-"${PACKAGE_VERSION}"
    needs:
        - build_docker_artifacts
    rules:
        - if: '$CI_COMMIT_BRANCH == "feature" && $CI_PIPELINE_SOURCE == "schedule"'
          when: never
        - if: '$CI_COMMIT_BRANCH == "feature"'

# --------------------------------------------------------------------
# Deploy pages

pages:
    tags:
        - gitlab-org-docker
    stage: deploy
    cache:
        policy: pull
    script:
        - echo "Deploy Gitlab pages..."
        - git config user.name "Gitlab CI Bot"
        - git config user.email "<ci@smilecdr.com>"
        - npm version --nogit-tag-version prepatch --preid=${CI_PIPELINE_ID}
        - npx nx run-many --target=build --all=true --configuration=staging
        - rm -rf public
        - mkdir -p public/quality
        - cp -r dist/apps/. public/
        - cp -r coverage public/
        - cp gitlab/index.html public/
        - cp gl-code-quality-report.html public/quality/
        - ls -alF public/
    environment:
        name: Pages Deployment
        url: https://simpatico.ai.gitlab.io/app-gallery-2021/
    artifacts:
        paths:
            - public/
    rules:
        - if: '$CI_COMMIT_BRANCH == "feature" && $CI_PIPELINE_SOURCE == "schedule"'
          when: never
        - if: '$CI_COMMIT_BRANCH == "feature"'

# --------------------------------------------------------------------
# Release jobs

release_patch_build:
    tags: 
        - lightweight
    stage: deploy
    cache:
        policy: pull
    before_script:
        - echo "Installing SSH key"
        - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )'
        - eval $(ssh-agent -s)
        - ssh-add <(echo "$AG_SSH_PRIVATE_KEY")
        - mkdir -p ~/.ssh
        - touch $HOME/.ssh/known_hosts
        - echo "$SSH_KNOWN_HOSTS" > $HOME/.ssh/known_hosts
    script:
        - echo "Create release..."
        - git config user.name "Gitlab CI Bot"
        - git config user.email "<ci@smilecdr.com>"
        - git checkout "$CI_COMMIT_BRANCH"
        - git fetch origin
        - git reset --hard origin/"$CI_COMMIT_BRANCH"
        - git status
        - npm install --force --save-dev @angular-devkit/build-angular
        - npm version --no-git-tag-version patch -m "Release $(date +%Y-%m-%d)"
        - npx nx run-many --target=build --all=true --configuration=production
        - git commit -am "adding new patch version"
        - SSH_HOST=$(echo "${CI_REPOSITORY_URL}" | sed -e 's|https\?://gitlab-ci-token:.*@|ssh://git@|g')
        - PACKAGE_VERSION=`cat package.json | grep "\"version\":" | cut -d "\"" -f 4`
        - PACKAGE_TAG=v"$PACKAGE_VERSION"
        - echo "Tagging release with ${PACKAGE_TAG} ${SMILE_VERSION}"
        - git tag -a $PACKAGE_TAG -m "AG UI version ${PACKAGE_TAG}"
        - git tag -a $SMILE_VERSION -m "Smile CDR - AG UI version match ${SMILE_VERSION}"
        - git tag | grep $PACKAGE_TAG
        - git tag | grep $SMILE_VERSION
        - git status
        - git remote rm origin && git remote add origin "$SSH_HOST"
        - GIT_SSH_COMMAND='ssh -o UserKnownHostsFile=$HOME/.ssh/known_hosts' git push --set-upstream origin "$CI_COMMIT_BRANCH"
        - GIT_SSH_COMMAND='ssh -o UserKnownHostsFile=$HOME/.ssh/known_hosts' git push origin $PACKAGE_TAG
        - GIT_SSH_COMMAND='ssh -o UserKnownHostsFile=$HOME/.ssh/known_hosts' git push origin $SMILE_VERSION
        - echo "Build webjar to maven repository..."
        - npx nx run-many --target=build --all=true --configuration=production
        - cd webjar
        - ./buildwebjar-appgallery.sh build ${SMILE_VERSION}
        - ./buildwebjar-marketplace.sh build ${SMILE_VERSION}
    artifacts:
        paths:
            - dist/
            - package.json
            - webjar/dist/cdr-appgallery-ui.jar
            - webjar/dist/cdr-marketplace-ui.jar
    when: manual
    rules:
        - if: '$CI_COMMIT_BRANCH == "master"'

release_patch_deploy:
    tags: 
        - lightweight
    stage: deploy
    image: maven:3.8.1-jdk-11
    before_script:
        - echo "Skipping global before_script"
    needs:
        - release_patch_build
    script:
        - echo "Deploy webjar to maven repository..."
        - cd webjar
        - ./buildwebjar-appgallery.sh deploy ${SMILE_VERSION}
        - ./buildwebjar-marketplace.sh deploy ${SMILE_VERSION}
    rules:
        - if: '$CI_COMMIT_BRANCH == "master"'
    needs:
        - release_patch_build
#test
